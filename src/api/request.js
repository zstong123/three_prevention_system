// import router from "../router/index"
import qs from 'qs'
import axios from 'axios'
import $ from 'jquery'

export const GET = (req) => {
	let headers = {
		'Content-Type': 'application/json;charset=utf-8',
		Authorization: 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1OTIxMTg5MDYxMDEsInBheWxvYWQiOiJ7XCJQU05fSURcIjpcIjhhOGE4YWUxNjgxMjcxMWMwMTY4MTJkMDRiYjkwMDc3XCIsXCJQU05fVU5cIjpcImdhb1wifSJ9.O0hSZBQn1Li8KIxDRro2krU-e-pUlpWvHcyQZtQ8MZA'
	}
	
	return axios.get(req.url, {
		params: req.data,
		headers
	}).then((res) => {
		// if (res.data.code != 0 || !sessionStorage.getItem('sessionKey')) {
		// 	router.replace({
		// 		path: '/'
		// 	})
		// }
		// console.log('')
		// console.info('%c' + req.title, 'font-size: 12px; font-family: Microsoft YaHei; font-weight: bold; color: #2953C3;')
		// console.log('请求接口：')
		// console.log(req.url)
		// console.log('请求参数：')
		// console.log(req.data)
		// console.log('返回数据：')
		// console.log(res.data)
		return Promise.resolve(res.data)
	})
}

export const POST = (req) => {
	return $.ajax({
		headers:{
			"Authorization": 'Bearer '+sessionStorage.token
		},
		url:req.url,
		data:JSON.stringify(req.data),
		contentType: "application/json;charset=utf-8",
		type:'post',
		dataType:'json',
		complete(res){
			return Promise.resolve(res.data)
		}
	})
/*	return axios.post(req.url, req.data, {
		transformRequest: data => qs.stringify(data),
=======
	let headers = {
		"Content-Type": 'application/json;charset=utf-8',
		"Authorization": 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1OTI1NzE1NzMwNjgsInBheWxvYWQiOiJ7XCJQU05fSURcIjpcIjhhOGE4YWUxNjgxMjcxMWMwMTY4MTJkMDRiYjkwMDc3XCIsXCJQU05fVU5cIjpcImdhb1wifSJ9.zHlg9udJWEgtlaiG7P226_y_hNjyiGsgT4kZk1Z50aw'
	}
	// headers["Authorization"] = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1OTE3NjA3OTEyNDMsInBheWxvYWQiOiJ7XCJQU05fSURcIjpcIjhhOGE4YWUxNjgxMjcxMWMwMTY4MTJkMDRiYjkwMDc3XCIsXCJQU05fVU5cIjpcImdhb1wifSJ9.ztT4eWOh2-iLrydhhdunbHZlWaxkIWgpS2TQLHi09Ik'
	return axios.post(req.url, req.data, {
		params: JSON.stringify(req.data),
		// transformRequest: data => qs.stringify(data),
>>>>>>> eefb67e2426355ce583d941d2230c279a6ca5687
		headers
	}).then((res) => {
		return Promise.resolve(res.data)
	})*/
}

export const UPLOAD = (req) => {
    let headers = {
        'Content-Type': 'multipart/form-data'
    }
    return axios.post(req.url, req.data, {
        headers
    }).then((res) => {
        console.info('%c' + req.title, 'font-size: 12px; font-family: Microsoft YaHei; font-weight: bold; color: #2953C3;')
		console.log(res.data)
		return Promise.resolve(res.data)
    })
}