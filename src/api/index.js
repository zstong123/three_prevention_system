import { GET, POST } from 'api/request.js'

class Axios {
	elements = data => POST({ title: '首页地图图标点', data, url: PREFIX + '/gis/elements'})
	riverPoi = data => POST({ title: '河道预警信息', data, url: PREFIX + '/gis/riverPoi'})
	reservoirPoi = data => POST({ title: '水库预警信息', data, url: PREFIX + '/gis/reservoirPoi'})
	waterPoi = data => POST({ title: '积水监测站', data, url: PREFIX + '/gis/waterPoi'}) //10秒刷新一次
	floodPoi = data => POST({ title: '易涝点', data, url: PREFIX + '/gis/floodPoi'})
	rainfallPoi = data => POST({ title: '1小时30mm', data, url: PREFIX + '/gis/rainfallPoi'})
	rainfallHour = data => POST({ title: '3小时50mm', data, url: PREFIX + '/gis/rainfall/hour3'})
	dutyInfo = data => POST({ title: '值班信息', data, url: PREFIX + '/gis/dutyinfo'})
	videoRecording = data => POST({ title: '融合通信录', data, url: PREFIX + '/gis/contacts3'})
	response = data => POST({ title: '应急响应', data, url: PREFIX + '/gis/plan/event3'})
	disasterDefStatistics = data => POST({ title: '防御情况', data, url: PREFIX + '/screen/disasterDefStatistics'})
	ranking = data => POST({ title: '街道降雨排行榜', data, url: PREFIX + '/gis/rainfall/ranking'})
	warningDetails = data => POST({ title: '警告事件详情', data, url: PREFIX + '/gis/event/get2'})

	eventPoi = data => POST({ title: '事件点', data, url: PREFIX + '/gis/eventPoi'})
	eventInf = data => POST({ title:'事件详情', data, url:PREFIX + '/gis/ele/get'})
	eleInfo = data => POST({ title:'应急资源', data, url:PREFIX + '/gis/ele/get'})
	
	riverPage = data => POST({ title: '河道水位站立时水位数据分页接口', data, url:PREFIX + '/gis/river/his/page'})
	riverDetail = data => POST({ title: '单个河道水位站详情', data, url:PREFIX + '/gis/river/get'})
	riverHistoryDetail = data => POST({ title: '单个河道水位站历史详情', data, url:PREFIX + '/gis/river/his'})
	waterDetail = data => POST({ title: '单个积水监测站详情', data, url:PREFIX + '/gis/water/get'})
	waterHistoryDetail = data => POST({ title: '单个积水监测站历史详情', data, url:PREFIX + '/gis/water/his'})
	waterPage = data => POST({ title: '积水监测站分页', data, url:PREFIX + '/gis/water/his/page'})
	rainfallDetail = data => POST({ title: '雨量站详情', data, url:PREFIX + '/gis/rainfall/get'})

	meetingCreate = data => POST({ title: '视频会议创建', data, url: PREFIX + '/gis/video/start'})
	meetingPerson = data => POST({ title: '入会人员列表', data, url: PREFIX + '/gis/video/status'})
	stopMeeting = data => POST({ title: '结束视频会议', data, url: PREFIX + '/gis/video/stop'})
	addMeeting = data => POST({ title: '会议成员邀请', data, url: PREFIX + '/gis/video/contacts'})
	speakToggle = data => POST({ title: '视频会议禁言，解禁', data, url: PREFIX + '/gis/video/gag'})
	allSpeakToggle = data => POST({ title: '视频会议一键禁言，解禁', data, url: PREFIX + '/gis/video/gag/oneclick'})
	qxyj = data => POST({ title: '气象预警数据', data, url: PREFIX + '/gis/qxyj'})
	callPhone = data => POST({ title: '单独拨打电话', data, url: PREFIX + '/gis/tel/call'})
	camera = data => POST({ title: '视频站播放流', data, url: PREFIX + '/gis/gmcc/camera'})
	floodget = data => POST({ title: '易涝点详情', data, url: PREFIX + '/gis/floodtip/get'})
	kick = data => POST({ title: '剔除', data, url: PREFIX + '/gis/video/kick'})
	login = data => POST({ title: '登录', data, url: PREFIX + '/user/login?PSN_UN=9080&PSN_PW=96e79218965eb72c92a549dd5a330112&APP_VERSION=1.1.5.9'})
	// login = data => POST({ title: '登录', data, url: PREFIX + '/user/login?PSN_UN=gao&PSN_PW=e10adc3949ba59abbe56e057f20f883e&APP_VERSION=1.1.5.9'})

}

export const axios = new Axios()
