import Vue from 'vue'
import App from './App.vue'
import router from "./router/index"

import '../src/assets/css/reset.css'
import '../src/assets/css/line-clamp.css'

// axios
import { axios } from "api/index.js"
Vue.prototype.$axios = axios

// echarts
import echarts from 'echarts'
Vue.prototype.$echarts = echarts

// ElementUI框架
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI)

import VueJsonp from 'vue-jsonp'
Vue.use(VueJsonp)

Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
  if (to.meta.title) {
      document.title = to.meta.title
  }
  next()
});

axios.login().then(v=>{
	// if (v.resultCode == 200 ) sessionStorage.token = v.user.token
  sessionStorage.token = v.resultCode == 200 ? v.user.token : getQueryString('token')
  function getQueryString(name) {
  	 let reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
  	 var r = window.location.search.substr(1).match(reg);
  
  	 if(r!=null)return  unescape(r[2]); return '';
  }
  new Vue({
    router,
    render: h => h(App),
  }).$mount('#app')
});

// getQueryString(name) {
// 	 let reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
// 	 var r = window.location.search.substr(1).match(reg);

// 	 if(r!=null)return  unescape(r[2]); return '';
// },