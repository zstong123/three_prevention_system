/**
 * 防御情况枚举伴生对象
 */
let map = new Map();
map.set('DEF_ISP_NUM_SUM', '巡查人数');
map.set('DEF_DEP_TSP_SUM', '出动车次');
map.set('DEF_STG_NUM_SUM', '施工设施加固（包括工棚、脚手架、塔吊等）个数');
map.set('DEF_HGH_NUM_SUM', '高空施工设备拆卸个数');
map.set('DEF_PIT_NUM_SUM', '深基坑和临时围墙警示牌设置个数');
map.set('DEF_ISP_LNE_SUM', '供电设备、线路巡查次数');
map.set('DEF_BBD_NUM_SUM', '加固广告牌、霓虹灯个数');
map.set('DEF_RMV_BBD_SUM', '拆卸广告牌、霓虹灯个数');
map.set('DEF_STG_TRE_SUM', '加固树木个数');
map.set('DEF_CUT_TRE_SUM', '修剪树木个数');
map.set('DEF_ISP_SLP_SUM', '危险边坡巡查次数');
map.set('DEF_WRN_SLP_SUM', '危险边坡警示牌设置个数');
map.set('DEF_ISP_SBY_SUM', '地铁口、立交桥底等易积水路段巡查次数');
map.set('DEF_ISP_PPE_SUM', '排水管网巡查次数');
map.set('DEF_ISP_RSV_SUM', '水库巡查次数');
map.set('DEF_DWN_RSV_SUM', '降低水位运行水库个数');
map.set('DEF_ISP_RVR_SUM', '河道巡查次数');
map.set('DEF_ISP_SWL_SUM', '海堤巡查次数');
map.set('DEF_ISP_WWR_SUM', '在建水务工程巡查次数');
map.set('DEF_ISP_PMP_SUM', '泵站、闸门等水利设施巡查次数');

// obj = [
// 	{key: 'DEF_ISP_NUM_SUM', name: '巡查人数', num: ''},
// 	{key: 'DEF_DEP_TSP_SUM', name: '出动车次', num: ''},
// 	{key: 'DEF_STG_NUM_SUM', name: '施工设施加固（包括工棚、脚手架、塔吊等）个数', num: ''},
// 	{key: 'DEF_HGH_NUM_SUM', name: '高空施工设备拆卸个数', num: ''},
// 	{key: 'DEF_PIT_NUM_SUM', name: '深基坑和临时围墙警示牌设置个数', num: ''},
// 	{key: 'DEF_ISP_LNE_SUM', name: '供电设备、线路巡查次数', num: ''},
// 	{key: 'DEF_BBD_NUM_SUM', name: '加固广告牌、霓虹灯个数', num: ''},
// 	{key: 'DEF_RMV_BBD_SUM', name: '拆卸广告牌、霓虹灯个数', num: ''},
// 	{key: 'DEF_STG_TRE_SUM', name: '加固树木个数', num: ''},
// 	{key: 'DEF_CUT_TRE_SUM', name: '修剪树木个数', num: ''},
// 	{key: 'DEF_ISP_SLP_SUM', name: '危险边坡巡查次数', num: ''},
// 	{key: 'DEF_WRN_SLP_SUM', name: '危险边坡警示牌设置个数', num: ''},
// 	{key: 'DEF_ISP_SBY_SUM', name: '地铁口、立交桥底等易积水路段巡查次数', num: ''},
// 	{key: 'DEF_ISP_PPE_SUM', name: '排水管网巡查次数', num: ''},
// 	{key: 'DEF_ISP_RSV_SUM', name: '水库巡查次数', num: ''},
// 	{key: 'DEF_DWN_RSV_SUM', name: '降低水位运行水库个数', num: ''},
// 	{key: 'DEF_ISP_RVR_SUM', name: '河道巡查次数', num: ''},
// 	{key: 'DEF_ISP_SWL_SUM', name: '海堤巡查次数', num: ''},
// 	{key: 'DEF_ISP_WWR_SUM', name: '在建水务工程巡查次数', num: ''},
// 	{key: 'DEF_ISP_PMP_SUM', name: '泵站、闸门等水利设施巡查次数', num: ''}
// ]

module.exports = {
  map,
}

