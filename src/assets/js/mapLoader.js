import {loadModules} from 'esri-loader';

let _this=this;
loadModules([
	"esri/Map",
	"esri/views/MapView",
	"esri/layers/TileLayer",
	"esri/Graphic",
	"esri/geometry/Point",
	"esri/symbols/PictureMarkerSymbol",
	"esri/layers/GraphicsLayer",
	"esri/layers/MapImageLayer",
	"dojo/domReady!"
], {url: 'api/init.js'}).then(([Map,MapView,TileLayer,Graphic]) => {
	//创建地图
	let map = new Map();
	//创建地图容器
	this.view = new MapView({
		container: "indexMap",
		map: map,
	});
	this.view.ui.remove('zoom');
	this.view.ui.remove('attribution');
	map.add(new TileLayer({
		url:"https://augurit.gzcc.gov.cn/server/rest/services/GZmap0304/MapServer"
	}));
	this.getData()
	// 2d地图加载后
	this.view.when(v=>{
		let arr=[
			{x:65723.97855578066,y:252641.55058411998},
			{x:78920.92300904455,y:168945.13774598183},
			{x:112955.14719624288,y:317931.6850456389},
			{x:109829.54870754594,y:253336.1197357583},
			{x:10505.190058828659,y:263754.7596027255},
		],arr1=[];
		arr.forEach((x,i)=>{
			let Graphic1 = new Graphic({
				geometry: {
					type: 'point',
					x:x.x,
					y:x.y,
					spatialReference:v.spatialReference
				},
				symbol: {
					type: 'picture-marker',
					format: "gif",
					url:"/warning.gif",
					width: '180px',
					height: '160px',
					params:'video'
				}
			});
			arr1.push(Graphic1)
		});
		//显示图标
		this.view.graphics.addMany(arr1);
	});

	this.view.on("click",v=>{
		this.view.hitTest({x: v.x, y: v.y}).then(response=>{
			let result = response.results[0];
			if (result) {
				_this.show_dialog[result.graphic.symbol.params]=true
			}
		});
	});
});