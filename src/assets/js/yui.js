export default {
    data(){
        return {
            iconId: '',
            constructiontList:[],
            slopeList:[],
            marketList:[],
            parkList:[],
            coastList:[],
            riverGateList:[],
            underParkList:[],
            billboardList:[],
            riskList: [],
            dangerousbuildingList:[],
            parkInfo: {
                navList: [
                    {name: '基本信息', isSel: true},
                   /* {name: '现在照片', isSel: false},*/
                ],
                imgList: [
                    {img: require('../images/1.png')},
                    {img: require('../images/2.png')},
                    {img: require('../images/3.png')},
                ]
            },
            coastInfo: {
                navList: [
                    {name: '基本信息', isSel: true},
                   /* {name: '现在照片', isSel: false},*/
                ],
                imgList: [
                    {img: require('../images/1.png')},
                    {img: require('../images/2.png')},
                    {img: require('../images/3.png')},
                ]
            },
            dangerousbuildingInfo: {
                navList: [
                    {name: '基本信息', isSel: true},
                    {name: '风险隐患', isSel: false},
                    {name: '管控措施', isSel: false},
                    {name: '整治效果', isSel: false},
                    {name: '各项责任', isSel: false},
                   /* {name: '现在照片', isSel: false},*/
                ],
                imgList: [
                    {img: require('../images/1.png')},
                    {img: require('../images/2.png')},
                    {img: require('../images/3.png')},
                ]
            },
            floodInfo: {
                navList: [
                    {name: '项目内容', isSel: true},
                    {name: '风险隐患', isSel: false},
                    {name: '管控措施', isSel: false},
                    {name: '整治效果', isSel: false},
                    {name: '各项责任', isSel: false},
                   /* {name: '现场照片', isSel: false},*/
                ],
                imgList: [
                    {img: require('../images/1.png')},
                    {img: require('../images/2.png')},
                    {img: require('../images/3.png')},
                ]
            },
            navIndex: 0,
            imgUrl: '',
            callsb:{
                displayName:'',
                phone:''
            },
            animate:false,
            disasterDefStatisticsList: [], // 防御情况
            tc:false,
            callName:'',
            callNum:'',
            qxInf:false,
            qxyjList:[{
                nm: '',
                tm: '',
                descr:'',
                lvl:'',
                type:''
            }]
        }
    },
    created(){
        // setInterval(this.scroll,1000)
		this.gData()
    },
    methods: {
        riskEvent(result){
            if(result.graphic.symbol.classes=='constructiont'){
				this.constructiontList = []
                this.$axios.eventInf({id:result.graphic.id}).then(v=>{
                    if(v.resultCode==200){
                        this.constructiontList=v.data;
                    }
                })
            }
            if(result.graphic.symbol.classes=='slope'){
				this.slopeList = []
                this.$axios.eventInf({id:result.graphic.id}).then(v=>{
                    if(v.resultCode==200){
                        this.slopeList=v.data;
                    }
                })
            }
            if(result.graphic.symbol.classes=='underMarket'){
				this.marketList = []
                this.$axios.eventInf({id:result.graphic.id}).then(v=>{
                    if(v.resultCode==200){
                        this.marketList=v.data;
                    }
                })
            }
            if(result.graphic.symbol.classes=='underPark'){
				this.underParkList = []
                this.$axios.eventInf({id:result.graphic.id}).then(v=>{
                    if(v.resultCode==200){
                        this.underParkList=v.data;
                    }
                })
            }
            if(result.graphic.symbol.classes=='park'){
				this.parkList = []
                this.$axios.eventInf({id:result.graphic.id}).then(v=>{
                    if(v.resultCode==200){
                        this.parkList=v.data;
                    }
                })
            }
            if(result.graphic.symbol.classes=='coast'){
				this.coastList = []
                this.$axios.eventInf({id:result.graphic.id}).then(v=>{
                    if(v.resultCode==200){
                        this.coastList=v.data;
                    }
                })
            }
            if(result.graphic.symbol.classes=='riverGate'){
				this.riverGateList = []
                this.$axios.eventInf({id:result.graphic.id}).then(v=>{
                    if(v.resultCode==200){
                        this.riverGateList=v.data;
                    }
                })
            }
            if(result.graphic.symbol.classes=='billboard'){
				this.billboardList = []
                this.$axios.eventInf({id:result.graphic.id}).then(v=>{
                    if(v.resultCode==200){
                        this.billboardList=v.data;
                    }
                })
            }
            if(result.graphic.symbol.classes=='dangerousbuilding'){
				this.dangerousbuildingList = []
                this.$axios.eventInf({id:result.graphic.id}).then(v=>{
                    if(v.resultCode==200){
                        this.dangerousbuildingList=v.data;
                    }
                })
            }
        },
        clickEvent(index) {
            this.navIndex = index
            for(let i = 0; i < this.parkInfo.navList.length; i++) {
                this.parkInfo.navList[i].isSel = i == index
            }
            for(let i = 0; i < this.coastInfo.navList.length; i++) {
                this.coastInfo.navList[i].isSel = i == index
            }
            for(let i = 0; i < this.dangerousbuildingInfo.navList.length; i++) {
                this.dangerousbuildingInfo.navList[i].isSel = i == index
            }
            for(let i = 0; i < this.floodInfo.navList.length; i++) {
                this.floodInfo.navList[i].isSel = i == index
            }
        },
        carouselEvent(url) {
            this.imgUrl = url
        },
        callSb(obj) {
            const arr = Object.keys(obj)
            const fzrArr = ['CONTACT','FZR','FZR2','FZR3','DH','contactphone','zlzrr','xczrr','ZLZRR']
            const map = new Map()
            map.set('CONTACT',{displayName:obj.CONTACT,phone:obj.CONTACTPHONE})
            map.set('FZR',{displayName:obj.FZR,phone:obj.FZRDH})
            map.set('FZR2',{displayName:obj.FZR2,phone:obj.FZRDH2})
            map.set('FZR3',{displayName:obj.FZR3,phone:obj.FZRDH3})
            map.set('DH',{phone:obj.DH})
            map.set('contact',{displayName:obj.contact,phone:obj.contactphone})
            map.set('zlzrr',{displayName:obj.zlzrr,phone:obj.zlzrrdh})
            map.set('xczrr',{displayName:obj.xczrr,phone:obj.xczrrdh})
            map.set('ZLZRR',{displayName:obj.ZLZRR,phone:obj.ZLZRRDH})
            arr.forEach( v => {
                if (fzrArr.includes(v)){
                    this.callTo(map.get(v))
                }
            })
        },
        gData(){
            this.$axios.disasterDefStatistics().then((res) => {
               /* console.log('123456',res.data)*/
                if (res.resultCode === 200) {
                    //zst
                    this.disasterDefStatisticsList = [
						{
							"name":'出动车次',
							"value":res.data[0].DEF_DEP_TSP_SUM
						},
						{
							"name":'施工设施加固个数',
							"value":res.data[0].DEF_STG_NUM_SUM
						},
						{
							"name":'高空施工设备拆卸个数',
							"value":res.data[0].DEF_HGH_NUM_SUM
						},
						{
							"name":'深基坑和临时围墙警示牌设置',
							"value":res.data[0].DEF_PIT_NUM_SUM
						},
						{
							"name":'供电设备、线路巡查次数',
							"value":res.data[0].DEF_ISP_LNE_SUM
						},
						{
							"name":'加固广告牌、霓虹灯',
							"value":res.data[0].DEF_BBD_NUM_SUM
						},
						{
							"name":'加固树木个数',
							"value":res.data[0].DEF_STG_TRE_SUM
						},
						{
							"name":'修建树枝',
							"value":res.data[0].DEF_CUT_TRE_SUM
						},
						{
							"name":'危险边坡巡查次数',
							"value":res.data[0].DEF_ISP_SLP_SUM
						},
                    ]
                }
            })
        },
        confirmCall(obj){
            console.log('123', obj)
            this.tc = !this.tc
            this.callName = obj.FZR ? obj.FZR : ''
            this.callNum = obj.FZRDH ? obj.FZRDH : ''
        },
        cancelConfirm(){
            console.log('123456789')
            this.tc = false
        },

        yjInf(obj){
            console.log('123456789')
            this.qxInf = !this.qxInf
            this.qxyjList.tm = obj.tm
            this.qxyjList.nm = obj.nm
            this.qxyjList.descr = obj.descr
            this.qxyjList.lvl = obj.lvl
            this.qxyjList.type = obj.type
        },
        closeyjInf(){
            this.qxInf = false
        }
    },
    computed:{
    }
}
