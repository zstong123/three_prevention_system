import echarts from 'echarts'
import flv from "./flv.min";
export default {
    data(){return {
        rain_radio:'线状图',
        rank_img:[
            require('../images/no1.png'),
            require('../images/no2.png'),
            require('../images/no3.png')
        ],
        active1:0,
        warning_index:0,
        warning:[],
        water_history:false,
		mapTypeIndex: -1,
        show_video:false,
		tableList: [],
        table_data:[{
            name:'水库',
            th:[
                {key: 'name', name: '站名'},
                {key: 'depth', name: '当前水位'},
                {key: 'total_storage_capacity', name: '当前库容'},
                {key: 'limited_water_level', name: '讯限水位'},
                {key: 'r_type', name: '类型'}
            ],
            td:[]
        },{
            name:'河道',
            th:[
                {key: 'name', name: '站名'},
                {key: 'depth', name: '当前水位'},
                {key: 'cjjsw', name: '超警戒水位'},
            ],
            td:[]
        },{
            name:'易涝点',
            th:[
                {key: 'name', name: '名称'},
                {key: 'depth', name: '当前水位'},
                {key: 'level', name: '预警级别'},
            ],
            td:[]
        }],
        player:{}
    } },
    created(){
        this.upAPI()
		this.timerUp()
    },
    methods: {
		timerUp() {
			setInterval(() => {
				this.upAPI()
			}, 60000);
		},
		upAPI() {
			this.$axios.qxyj().then(v=>{
			    if(v.resultCode==200){
			        this.warning=v.data
			    }
			})
		},
        videoTo(x){
            this.$refs.meet.show=true;
            if(x){
                this.$refs.meet.meetingCreate(x,id=>{
                    this.$axios.addMeeting({CONFERENCEID: id, USERS:[x]}).then(v => {
                        if (v.resultCode != 200) {
                            this.$message.error(v.resultMsg)
                        }
                    })
                });
            }else{
                this.$refs.meet.meetingCreate()
            }
        },
        percent(v){
            let a=this.ranking.length>0?this.ranking[0].rainfall:0;
            if(!a) return 0;
            return (v*100/a).toFixed(2)
        },
        setVideo(czbm,cgqbh){
            this.$axios.camera({CZBM:czbm,cgqbh:cgqbh || 0}).then(v=>{
                if(v.resultCode==200){
                    this.player = flv.createPlayer({
                        url:v.data.url,
                        isLive:true,
                        cors:true,
                        type:'flv'
                    });
                    this.$nextTick(e=>{
                        this.player.attachMediaElement(this.$refs.show_video)
                        this.player.load()
                        this.player.play()
                    })
                }
            })
        },
        callTo(x){
            let a=confirm(`确认拨打电话：\n【${x.displayName || x.zbry}】`);
            if (a){
                this.$axios.callPhone({PHONE:x.phone || x.lxfs}).then(v=>{
                    if(v.resultCode==200){
                        this.$refs.alert.show_call=true
                        this.$refs.alert.name=x.displayName|| x.zbry;
                    }
                })
            }
        }
    },
    computed:{
        warning_obj(){
            return this.warning||{}
        }
    },
    watch:{
        'show_dialog.river':{
            immediate:true,
            handler(v){
                if(!v) return ;
                this.$nextTick(e=>{
                    let myCharts=echarts.init(this.$refs.river_charts,'dark');
                    myCharts.setOption({
                        textStyle:{fontSize:12*RATIO},
                        backgroundColor: 'transparent',
                        tooltip : {
                            trigger: 'axis',
                            padding:5*RATIO,
                            textStyle:{
                                fontSize:14*RATIO
                            }
                        },
                        grid: {
                            top:'15%',
                            left: '3%',
                            right: '3%',
                            bottom: '1%',
                            containLabel: true
                        },
                        calculable : true,
                        xAxis: {
                            type: 'category',
                            axisTick: {show:false},
                            axisLabel:{
                                fontSize:12*RATIO,
                                lineHeight:20*RATIO,
                            },
                            axisLine: {
                                lineStyle:{
                                    width:1*RATIO
                                }
                            },
                            data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
                        },
                        yAxis: {
                            nameTextStyle:{
                                lineHeight:35*RATIO
                            },
                            name:'水位 (m)',
                            type: 'value',
                            axisLine: {
                                show:false
                            },
                            axisTick: {show:false},
                            axisLabel:{
                                fontSize:12*RATIO
                            },
                            splitLine:{
                                lineStyle:{width:1*RATIO}
                            },
                        },
                        series: [{
                            symbol:'circle',
                            symbolSize:6*RATIO,
                            data: [820, 932, 901, 934, 1290, 1330, 1320],
                            type: 'line',
                            itemStyle : {
                                normal : {
                                    color:'#f08a36',
                                    lineStyle:{
                                        width:1*RATIO,
                                        color:'#0ac4ce'
                                    }
                                }
                            }
                        }]
                    },true)
                })
            }
        },
        'show_dialog.waterLv':{
            immediate:true,
            handler(v){
                if(!v) return ;
                this.$nextTick(e=>{
                    let myCharts=echarts.init(this.$refs.water_charts,'dark');
                    myCharts.setOption({
                        textStyle:{fontSize:12*RATIO},
                        backgroundColor: 'transparent',
                        tooltip : {
                            trigger: 'axis',
                            padding:5*RATIO,
                            textStyle:{
                                fontSize:14*RATIO
                            }
                        },
                        grid: {
                            top:'15%',
                            left: '3%',
                            right: '13%',
                            bottom: '1%',
                            containLabel: true
                        },
                        calculable : true,
                        xAxis: {
                            type: 'category',
                            axisTick: {show:false},
                            axisLabel:{
                                fontSize:12*RATIO,
                                lineHeight:20*RATIO,
                            },
                            axisLine: {
                                lineStyle:{
                                    width:1*RATIO
                                }
                            },
                            data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
                        },
                        yAxis: {
                            nameTextStyle:{
                                lineHeight:35*RATIO
                            },
                            name:'积水深度 (cm)',
                            type: 'value',
                            axisLine: {
                                show:false
                            },
                            axisTick: {show:false},
                            axisLabel:{
                                fontSize:12*RATIO
                            },
                            splitLine:{
                                lineStyle:{width:1*RATIO}
                            },
                        },
                        series: [{
                            symbol:'circle',
                            symbolSize:6*RATIO,
                            data: [820, 932, 901, 934, 1290, 1330, 1320],
                            type: 'line',
                            smooth: true,
                            itemStyle : {
                                normal : {
                                    color:'#f08a36',
                                    lineStyle:{
                                        width:1*RATIO,
                                        color:'#0ac4ce'
                                    }
                                }
                            },
                            markLine: {
                                lineStyle:{
                                    width:1*RATIO
                                },
                                itemStyle:{
                                    normal:{
                                        label:{
                                            formatter:'{b}'
                                        }
                                    }
                                },
                                data: [
                                    {name:'严重内涝警戒',yAxis:600,lineStyle:{color:'red'}},
                                    {name:'轻微内涝警戒',yAxis:300,lineStyle:{color:'orange'}},
                                    {name:'轻微积水警戒',yAxis:100,lineStyle:{color:'yellow'}},
                                ]
                            }
                        }]
                    },true)
                })
            }
        },
        'show_dialog.rain':{
            immediate:true,
            handler(v){
                if(!v) return ;
                this.$nextTick(e=>{
                    let myCharts=echarts.init(this.$refs.rain_charts,'dark');
                    myCharts.setOption({
                        backgroundColor: 'transparent',
                        tooltip : {trigger: 'axis'},
                        grid: {
                            top:'10%',
                            left: '3%',
                            right: '1%',
                            bottom: '1%',
                            containLabel: true
                        },
                        calculable : true,
                        xAxis: {
                            name:'当前时间',
                            type: 'category',
                            axisTick: {show:false},
                            axisLabel:{
                                textStyle:{color:'#0ac4ce'}
                            },
                            axisLine: {
                                lineStyle:{color:'#0ac4ce'}
                            },
                            data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
                        },
                        yAxis: {
                            nameTextStyle:{
                                color:'#0ac4ce'
                            },
                            name:'雨量 (mm)',
                            type: 'value',
                            axisLine: {
                                show:false
                            },
                            axisTick: {show:false},
                            axisLabel:{
                                textStyle:{color:'#0ac4ce'}
                            },
                            splitLine:{
                                lineStyle:{color:'#0ac4ce'}
                            },
                        },
                        series: [{
                            symbol:'circle',
                            symbolSize:6,
                            data: [820, 932, 901, 934, 1290, 1330, 1320],
                            type: 'line',
                            smooth: true,
                            itemStyle : {
                                normal : {
                                    color:'#f08a36',
                                    lineStyle:{
                                        color:'#0ac4ce'
                                    }
                                }
                            }
                        }]
                    },true)
                })
            }
        }
    }
}
