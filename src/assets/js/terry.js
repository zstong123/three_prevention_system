import echarts from 'echarts'
import flv from "./flv.min"

export default {
    data() {
        return {
            person: {},
            medical: {},
            shelter: {},
            team: {},
            material: {},
            pipe: {},
            river: {},
            riverId: '',
            reservoir: {},
            water: {},
            waterId: '',
            rainfall: {},
            rainfallList: [],
            rainsTotal: 0,
            defaultXData: [0, 10, 20, 30, 40, 50],
            defaultYData: [],
            reservoirLine: [
                {
                    name: '校核洪水位',
                    par: 'CHECK_WATER_LEVEL',
                    yAxis: 0,
                    lineStyle: {color: 'rgba(189,190,191,1)'},
                    label: {fontSize: 15 * RATIO, color: 'rgba(189,190,191,1)'}
                },
                {
                    name: '汛限水位',
                    par: 'LIMITED_WATER_LEVEL',
                    yAxis: 0,
                    lineStyle: {color: 'rgba(173,7,13,1)'},
                    label: {fontSize: 15 * RATIO, color: 'rgba(173,7,13,1)'}
                },
                {
                    name: '当前水位',
                    par: 'DEPTH',
                    yAxis: 0,
                    lineStyle: {color: 'rgba(20,55,94,1)'},
                    label: {fontSize: 15 * RATIO, color: 'rgba(20,55,94,1)'}
                },
                {
                    name: '死水位',
                    par: 'SSW',
                    yAxis: 0,
                    lineStyle: {color: 'rgba(155,147,77,1)'},
                    label: {fontSize: 15 * RATIO, color: 'rgba(155,147,77,1)'}
                }
            ],
            showWaterHistory: true
        }
    },

    methods: {
        // ele对应图标的ref
        // 水库图
        reservoirChart(ele ,markline) {
            let myCharts = echarts.init(this.$refs[ele], 'dark');
            let markline_data=[];
            let arr=[
                {color:'#FFF',name:'校核洪水位'},
                {color:'#FF473A',name:'汛限水位'},
                {color:'#197edf',name:'当前水位'},
                {color:'#FF0',name:'设计洪水位'}
            ];
            arr.forEach((x,i)=>{
                markline_data.push({
                    label: {
                        position: 'middle',
                        fontSize:12*RATIO,
                        formatter: '{b}：{c}m',
                        color:x.color
                    },
                    name:x.name,
                    yAxis:markline[i],
                    lineStyle:{
                        type:"solid",
                        color:x.color,
                        width:RATIO
                    }
                })
            });
            myCharts.setOption({
                textStyle: {fontSize: 2 * RATIO},
                backgroundColor: 'transparent',
                calculable: true,
                xAxis: {
                    type: 'category',
                    data: [''],
                    axisTick: {show: false},
                    axisLabel: {
                        fontSize: 13 * RATIO,
                        lineHeight: 13 * RATIO,
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#FFF',
                            width: RATIO
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#FFF',
                        }
                    }
                },
                yAxis: {
                    nameTextStyle: {
                        lineHeight: RATIO,
                        fontSize: 13 * RATIO
                    },
                    max:50,
                    min:30,
                    boundaryGap: [1, 1],
                    type: 'value',
                    axisLine: {
                        show: true,
                        lineStyle: {
                            color: '#FFF',
                            width:RATIO
                        }
                    },
                    axisTick: {show: true},
                    axisLabel: {
                        fontSize: 13 * RATIO,
                    },
                    splitLine: {
                        show: false,
                        lineStyle: {
                            color: '#FFF',
                            type: 'dotted',
                            opacity: .02,
                            width:RATIO
                        }
                    },
                    splitNumber: 5
                },
                grid: {
                    top: 50*RATIO,
                    right:0,
                    left:30*RATIO,
                    bottom:10*RATIO,
                },
                graphic: {
                    elements: [
                        {
                            type: 'image',
                            right:0,
                            top:50*RATIO,
                            z: 5,
                            bounding: 'raw',
                            origin: [0, 0],
                            style: {
                                image: require('../images/img.png'),
                                width:80*RATIO,
                                height: 260*RATIO,
                                opacity: .9
                            }
                        },
                        {
                            type: 'text',
                            right:0,
                            top:25*RATIO,
                            z: 5,
                            style: {
                                text: '堤坝高度：50m',
                                fill: '#FFF',
                                fontSize:13*RATIO
                            }
                        }
                    ],

                },
                series: [{
                    type: 'bar',
                    data:[markline[2]],
                    barCategoryGap: '-100%',
                    itemStyle: {
                        color: new echarts.graphic.LinearGradient(0, 0, 0, 1,[{offset: 0, color: '#0493df'}, {offset: 1, color: '#0345a3'}])
                    },
                    markLine: {
                        data: markline_data
                    }
                }]
            }, true)
        },
        changeRainRadio(v) {

        },
        // 雨量站
        // rain_charts
        rainCharts(ele, xData, yData) {
            let myCharts = echarts.init(this.$refs[ele], 'dark');
            myCharts.setOption({
                textStyle: {fontSize: 20 * RATIO},
                backgroundColor: 'transparent',
                tooltip: {
                    trigger: 'axis',
                    padding: 10 * RATIO,
                    textStyle: {
                        fontSize: 20 * RATIO
                    }
                },
                calculable: true,
                xAxis: {
                    type: 'category',
                    offset: 5 * RATIO,
                    axisTick: {show: false},
                    axisLabel: {
                        fontSize: 15 * RATIO,
                        lineHeight: 15 * RATIO,
                    },
                    axisLine: {
                        lineStyle: {
                            width: 1 * RATIO
                        }
                    },
                    data: xData
                },
                yAxis: {
                    nameTextStyle: {
                        lineHeight: 10 * RATIO,
                        fontSize: 15 * RATIO
                    },
                    offset: 5 * RATIO,
                    max: function (value) {
                        return Math.ceil(value.max)
                    },
                    min: 0,
                    boundaryGap: [1, 1],
                    type: 'value',
                    axisLine: {
                        show: true
                    },
                    axisTick: {show: true},
                    axisLabel: {
                        fontSize: 15 * RATIO
                    },
                    splitLine: {
                        lineStyle: {
                            color: ['#eee'],
                            type: 'dotted',
                            width: 1 * RATIO
                        }
                    },
                    splitNumber: 5
                },
                series: [{
                    symbol: 'circle',
                    symbolSize: 10 * RATIO,
                    smooth: true,
                    data: yData,
                    type: 'line',
                    areaStyle: {},
                    itemStyle: {
                        normal: {
                            color: '#f08a36',
                            lineStyle: {
                                width: 1 * RATIO,
                                color: '#0ac4ce'
                            }
                        }
                    },
                    markLine: {
                        lineStyle: {
                            width: 2 * RATIO,
                            type: 'dotted'
                        },
                        label: {fontSize: 20 * RATIO},
                        itemStyle: {
                            normal: {
                                label: {
                                    position: 'middle',
                                    formatter: '{b}:{c}m'
                                }
                            }
                        },
                        data: []
                    }
                }]
            }, true)
        },
        // 打电话
        call(val) {
            const arr = Object.keys(val)
            const fzrArr = ['FZR', 'FZR2', 'FZR3', 'UNITDH', 'YJDH']
            const map = new Map()
            map.set('FZR', {displayName: val.FZR, phone: val.FZRDH})
            map.set('FZR2', {displayName: val.FZR2, phone: val.FZRDH2})
            map.set('FZR3', {displayName: val.FZR3, phone: val.FZRDH3})
            map.set('UNITDH', {displayName: val.UNIT, phone: val.UNITDH})
            map.set('YJDH', {displayName: val.YJDH, phone: val.YJDH})
            arr.forEach(v => {
                if (fzrArr.includes(v)) {
                    this.callTo(map.get(v))
                }
            })
        },
        seekPlace(result) {
            this.rain_radio = '线状图'
            // 应急资源
            if (result.graphic.symbol.classes == 'person') {
                this.$axios.eleInfo({id: result.graphic.id}).then(v => {
                    if (v.resultCode == 200) {
                        console.log('人员定位::', v)
                        this.person = v.data;
                    }
                })
            }
            if (result.graphic.symbol.classes == 'medical') {
                this.$axios.eleInfo({id: result.graphic.id}).then(v => {
                    if (v.resultCode == 200) {
                        console.log('医疗机构::', v)
                        this.medical = v.data;
                    }
                })
            }
            if (result.graphic.symbol.classes == 'shelter') {
                this.$axios.eleInfo({id: result.graphic.id}).then(v => {
                    if (v.resultCode == 200) {
                        console.log('避难场所::', v)
                        this.shelter = v.data;
                    }
                })
            }
            if (result.graphic.symbol.classes == 'team') {
                this.$axios.eleInfo({id: result.graphic.id}).then(v => {
                    if (v.resultCode == 200) {
                        console.log('应急队伍::', v)
                        this.team = v.data;
                    }
                })
            }
            if (result.graphic.symbol.classes == 'material') {
                this.$axios.eleInfo({id: result.graphic.id}).then(v => {
                    if (v.resultCode == 200) {
                        console.log('物资储备站::', v)
                        this.material = v.data;
                    }
                })
            }

            // 监测点
            if (result.graphic.symbol.classes == 'pipe') {
                this.$axios.eleInfo({id: result.graphic.id}).then(v => {
                    if (v.resultCode == 200) {
                        console.log('管网测点::', v)
                        this.pipe = v.data;
                    }
                })
            }
            if (result.graphic.symbol.classes == 'river') {
                this.$axios.riverDetail({id: result.graphic.id}).then(v => {
                    if (v.resultCode == 200) {
                        this.river = v.data;
                        this.riverId = result.graphic.id
                        console.log('河道水位站图表:::', v)
                    }
                })
            }
            if (result.graphic.symbol.classes == 'reservoir') {
                this.$axios.eleInfo({id: result.graphic.id}).then(v => {
                    if (v.resultCode == 200) {
                        let data = v.data;
						this.reservoir = v.data;
                        this.reservoirChart('reservoir_charts', [
                            data.CHECK_WATER_LEVEL || 0,
                            data.LIMITED_WATER_LEVEL || 0,
                            data.DQSW || 0,
                            data.WATER_LEVEL || 0,
                        ]);
                    }
                })
            }
            if (result.graphic.symbol.classes == 'water') {
				// this.$axios.waterPoi({id: result.graphic.id}).then(v => {
                this.$axios.waterDetail({id: result.graphic.id}).then(v => {
                    if (v.resultCode == 200) {
                        this.water = v.data;
                        this.waterId = result.graphic.id
                        console.log('积水监测点::', v)
                    }
                })
            }
            if (result.graphic.symbol.classes == 'rainfall') {
                this.$axios.rainfallDetail({id: result.graphic.id}).then(v => {
                    if (v.resultCode == 200) {
                        console.log('雨量站::', v)
                        this.rainfall = v.data;
                        const xAxis = this.rainfall.rainfalls.map(v => v.uptime)
                        const yAxis = this.rainfall.rainfalls.map(v => v.rain)
                        this.rainfallList = v.data.rainfalls
                        this.rainsTotal = this.rainfallList.reduce((prev, next) => prev + next.rain, 0)
                        this.rainCharts('rain_charts', xAxis, yAxis)
                    }
                })
            }
        }
    },
    created() {
        // this.getRiverData()

    }
}
