export default {
    data(){
		return {
			esri: {},
			serverMapList: [],  // 图标存储
			allServerMapList : [], //所有图标存储
			serverMapListCopy: [],
			delMapList: [],
			addOneIcontemp: [], // 添加单个图标
			mapList: [],
			dataObj: {},
			wGraphic: {},
			warningGraphic: {},
			warningDetails: null,
			timeEventList: [],
			sliderLevel: 0, // 滑块等级
			circleGraphicsLayer: {}, // 画圆
			graphicTiem: null, // 事件点时间
			eventGraphicList: [], // 警告圈事件数组
			isPopover: false, // 是否显示点位监测预警信息
			leftV: 0,		// 点位监测预警信息箭头位置
			spotW: 29,		// 事件滑块点的距离
			circleW: 500,	// 圆圈半径
			scaleV: RATIO,	// 页面缩放
			elementList: [], // 全图层所有元素POI信息列表
			riverPoiList: [],	// 河道预警信息
			riverPoiLevel: 0,   // 河道预警等级
			riverPoiLevel1: 0,   // 河道预警等级
			riverPoiLevel2: 0,   // 河道预警等级
			riverPoiLevel3: 0,   // 河道预警等级
			reservoirPoiList: [], // 水库预警信息
			reservoirPoiLevel: 0,   // 水库预警等级
			reservoirPoiLevel1: 0,
			reservoirPoiLevel2: 0,
			reservoirPoiLevel3: 0,
			floodData: {},		// 易涝点图标数据
			floodGraphic: {},	// 易涝点图标图层
			floodPoiList: [],	// 易涝信息
			floodPoiLevel: 0,   // 易涝预警等级
			floodPoiLevel1: 0,   // 易涝预警等级
			floodPoiLevel2: 0,   // 易涝预警等级
			floodPoiLevel3: 0,   // 易涝预警等级
			rainfallPoiList: [], // 1小时30mm
			rainfallPoiLevel: 0, // 1小时30mm预警等级
			rainfallHourList: [], // 3小时50mm
			rainfallHourLevel: 0, // 3小时50mm预警等级
			dutyList: {},   // 值班信息
			responseList: [], // 应急响应
			videoRecordingList: [], // 视频会议通信录
			/*disasterDefStatisticsList: [], // 防御情况*/
			titleList: [
				{ key: 'name', name: '站名' },
				{ key: 'address', name: '所属区域' },
				{ key: 'depth', name: '当前水位' },
				{ key: 'level', name: '预警级别' },
				{ key: 'uptime', name: '预警时间' },
			],
			warningLevelList: [],
			navTxt: '首页',
			isSuperposition: false,
			mapSignList: [
				{
					name: '风险点',
					isSelect: false,
					Children: [
						{ name: '在建工地', isSelect: false, icon: require('../images/mapIcon/1-1.png'), classes: 'constructiont' },
						{ name: '大型广告牌', isSelect: false, icon: require('../images/mapIcon/1-2.png'), classes: 'billboard' },
						{ name: '危险边坡', isSelect: false, icon: require('../images/mapIcon/1-3.png'), classes: 'slope' },
						{ name: '易涝积水', isSelect: true, icon: require('../images/mapIcon/1-4.png'), classes: 'flood' }, // 单独获取数据
						{ name: '地下商场', isSelect: false, icon: require('../images/mapIcon/1-5.png'), classes: 'underMarket' },
						{ name: '地下停车场', isSelect: false, icon: require('../images/mapIcon/1-6.png'), classes: 'underPark' },
						{ name: '公园', isSelect: false, icon: require('../images/mapIcon/1-7.png'), classes: 'park' },
						{ name: '重点防护点', isSelect: false, icon: require('../images/mapIcon/1-8.png'), classes: 'coast' },
						{ name: '危房', isSelect: false, icon: require('../images/mapIcon/1-9.png'), classes: 'dangerousbuilding' },
						{ name: '河道闸门', isSelect: false, icon: require('../images/mapIcon/1-10.png'), classes: 'riverGate' },
					]
				},
				{
					name: '应急资源',
					isSelect: false,
					Children: [
						{ name: '避难场所', isSelect: false, icon: require('../images/mapIcon/2-1.png'), classes: 'shelter' },
						{ name: '医疗机构', isSelect: false, icon: require('../images/mapIcon/2-2.png'), classes: 'medical' },
						{ name: '应急队伍', isSelect: false, icon: require('../images/mapIcon/2-3.png'), classes: 'team' },
						{ name: '物资储备站', isSelect: false, icon: require('../images/mapIcon/2-4.png'), classes: 'material' },
						// { name: '人员定位', isSelect: false, icon: require('../images/mapIcon/2-5.png'), classes: 'person' },  // 单独获取数据
					]
				},
				{
					name: '监测点',
					isSelect: false,
					Children: [
						// { name: '管网测点', isSelect: false, icon: require('../images/mapIcon/3-1.png'), classes: 'pipe' },
						{ name: '河道水位站', isSelect: false, icon: require('../images/mapIcon/3-2.png'), classes: 'river' },
						{ name: '水库', isSelect: false, icon: require('../images/mapIcon/3-3.png'), classes: 'reservoir' },
						{ name: '积水监测点', isSelect: false, icon: require('../images/mapIcon/3-4.png'), classes: 'water' },
						{ name: '雨量站', isSelect: false, icon: require('../images/mapIcon/3-5.png'), classes: 'rainfall' },
					]
				},
				{
					name: '视频监控',
					isSelect: false,
					Children: [
						{ name: '易涝点监控', isSelect: true, icon: require('../images/mapIcon/4-1.png'), classes: 'video', splb: '0' },
						{ name: '主要路段监控', isSelect: false, icon: require('../images/mapIcon/4-1.png'), classes: 'video', splb: '2' },
						{ name: '交叉路口监控', isSelect: false, icon: require('../images/mapIcon/4-1.png'), classes: 'video', splb: '1' },
						{ name: '水库河道监控', isSelect: false, icon: require('../images/mapIcon/4-1.png'), classes: 'video', splb: '3' },
						{ name: '其他', isSelect: false, icon: require('../images/mapIcon/4-1.png'), classes: 'video', splb: '99' },
					]
				},
			],

			siteInfo: {
				navList: [
					{ name: '基本信息', isSel: true },
					{ name: '现在照片', isSel: false },
				],
				imgList: [
					{ img: require('../images/1.png') },
					{ img: require('../images/2.png') },
					{ img: require('../images/3.png') },
				]
			},
			ranking:[],
			isfirst:true,
			flood_data:{}
		}
	},

	created() {
		this.getSecondsAPI()
		this.getBranchAPI()
		this.secondsUp()
		this.branchUp()

	},
	methods: {
		secondsUp() {
			setInterval(() => {
				this.getSecondsAPI()
			}, 10000);
		},

		branchUp() {
			setInterval(() => {
				this.getBranchAPI()
			}, 60000);
		},
		// 每分钟需要刷新的接口
		getBranchAPI() {
			// 1小时30mm
			this.$axios.rainfallPoi().then((res) => {
				if (res.resultCode === 200) {
					this.rainfallPoiList = res.data
					// this.rainfallPoiLevel=this.rainfallPoiList.filter(o=>o.level>0).length;
					this.rainfallPoiList = this.getRainfallNum(this.rainfallPoiList)
				}
			})
			// 3小时50mm
			this.$axios.rainfallHour().then((res) => {
				if (res.resultCode === 200) {
					this.rainfallHourList = res.data
					// this.rainfallHourLevel=this.rainfallHourList.filter(o=>o.level>0).length;
				}
			})
			// 获取值班信息
			this.$axios.dutyInfo().then((res) => {
				if (res.resultCode === 200) {
					this.dutyList = res.data
				}
			})
			// 获取融合通信录
			this.$axios.videoRecording().then((res) => {
				if (res.resultCode === 200) {
					this.videoRecordingList = res.data
				}
			})

			// 应急响应
			this.$axios.response().then((res) => {
				if (res.resultCode === 200) {
					this.responseList = res.data
				}
			})
			// 降雨排行榜
			this.$axios.ranking().then(v=>{
				if (v.resultCode === 200) {
					this.ranking=v.data
				}
			});
		},
		getSecondsAPI() {
			// 获取河道预警信息
			this.$axios.riverPoi().then((res) => {
				if (res.resultCode === 200) {
					this.riverPoiList = res.data

					this.riverPoiLevel=this.riverPoiList.filter(o=>o.level>0).length;
					this.riverPoiLevel1=this.riverPoiList.filter(o=>o.level==1).length;
					this.riverPoiLevel2=this.riverPoiList.filter(o=>o.level==2).length;
					this.riverPoiLevel3=this.riverPoiList.filter(o=>o.level==3).length;
				}
			})
			// 获取水库预警信息
			this.$axios.reservoirPoi().then((res) => {
				if (res.resultCode === 200) {
					this.reservoirPoiList = res.data
					this.onWaterRegime(0)
					// this.tableList = this.reservoirPoiList
					this.reservoirPoiLevel=this.reservoirPoiList.filter(o=>o.level>0).length
					this.reservoirPoiLevel1=this.reservoirPoiList.filter(o=>o.level==1).length
					this.reservoirPoiLevel2=this.reservoirPoiList.filter(o=>o.level==2).length
					this.reservoirPoiLevel3=this.reservoirPoiList.filter(o=>o.level==3).length
				}
			})
			// 获取易涝点预警信息
			this.$axios.floodPoi().then((res) => {
				if (res.resultCode === 200) {
					this.floodPoiList = res.data
					this.floodPoiLevel=this.floodPoiList.filter(o=>o.level>0).length;
					this.floodPoiLevel1=this.floodPoiList.filter(o=>o.level==1).length;
					this.floodPoiLevel2=this.floodPoiList.filter(o=>o.level==2).length;
					this.floodPoiLevel3=this.floodPoiList.filter(o=>o.level==3).length;
				}
			})
		},
		getTimeEvent() {
			this.$axios.eventPoi().then((res) => {
				if (res.resultCode === 200) {
					this.timeEventList = res.data
					let list = this.getXY(res.data)
					this.addCircle(list)

				}
			})
		},
		floodNearbyEvent() {
			this.$refs.flood.show=false;
			this.removegGhicsLayer()
			this.spotEvent('warning', 29, 500)
			this.navTxt = 'floodEvent'
			this.circleW = 500
			this.addCircleGeometry(this.floodGraphic, 500)
			// console.log('warningGraphic', this.warningGraphic)
		},
		addCircle(list) {
			let that = this
			let warningId = ''
			this.view.when(v=>{
				list.forEach((i) => {
					let Graphic2 = new this.esri.Graphic({
						geometry: {
							type: 'point',
							x: i.longitude,
							y: i.latitude,
							spatialReference:v.spatialReference
						},
						symbol: {
							type: "picture-marker",
							url: './warning.png',
							width: '140px',
							height: '120px',
							warning: true,
							classes: 'warning',
							// data: data,
						},
						id: i.id,

					});
					warningId = i.id
					this.pointFalsh(Graphic2);
					this.eventGraphicList.push(Graphic2)

				})


				this.view.graphics.addMany(this.eventGraphicList);
			})
				this.view.on("click",v=>{
					this.floodGraphic = {}
				    this.view.hitTest({x: v.x, y: v.y}).then(response=>{
				        let result = response.results[0];

				        if (result) {

							if (result.graphic.symbol.classes == 'flood') {
								this.floodGraphic = result.graphic
								if (this.circleGraphicsLayer.graphics.items <= 0) {
									this.floodGraphic = result.graphic
									this.floodData = result.graphic.symbol.data
									// console.log('floodData', this.floodData)
								}
							}
							if (result.graphic.symbol.classes == 'warning') {
								// console.log('warningId', that.eventGraphicList)
								that.eventGraphicList.forEach((i) => {
									i.visible = i.id == result.graphic.id
								})

								that.$axios.warningDetails({id:result.graphic.id}).then(v=>{
								    if(v.resultCode==200){
										// that.removegGhicsLayer()
										// console.log('warningDetails', v.data)
								        that.warningDetails = v.data
										that.navTxt = '事件'
										if (this.circleGraphicsLayer.graphics.items <= 0) {
											this.wGraphic = result.graphic
											that.circleW = 500
											that.addCircleGeometry(result.graphic, 500)
										}

								    }
								})
							}
						}
					})
				})
		},

		addCircleGeometry(data, num) {
			let that = this
			this.circleGraphicsLayer.remove(this.warningGraphic)
			// console.log('画圆数据', data)
			that.view.when(v=>{
				var point = {
				    type: "point",
				    x: data.geometry.x,
				    y: data.geometry.y,
				    spatialReference: v.spatialReference
				};
				var symbol = {
				    type: "simple-fill",
				    color: [53,24,45,.5],
				    style: "solid",
				    outline: {
				        color: [160,12,14],
				        width: 1
				    }
				};
				let circle = new this.esri.CircleGeometry({
				    center: point,
				    // radius: that.circleW,
					radius: num,
					// radiusUnit: "kilometers",
				    spatialReference: v.spatialReference
				});

				let graphic = new that.esri.Graphic({
				    geometry: circle,
				    symbol: symbol
				});
				this.warningGraphic = graphic
				this.circleGraphicsLayer.add(this.warningGraphic);

				let geometryEngine = this.esri.GeometryEngine

				this.view.graphics.removeAll()
				let currentMapList = []
				this.allServerMapList.forEach((i, k) => {
					if(geometryEngine.contains(circle, i.geometry)){
						currentMapList.push(i)
					}
				})

				// 读取事件图标集合
				this.getTimeEvent()
				if(currentMapList.length > 0)this.view.graphics.addMany(currentMapList);

			})
		},
		pointFalsh(graphic) {
			//zst
		    let temp = 0;
			this.graphicTiem = setInterval(() => {
			    if (graphic.visible) {
					graphic.visible = false;
				} else {
					graphic.visible = true;
				}
			    temp++;
			}, 400);
		},
		setMapIcon(list) {
			this.view.graphics.removeAll()
			this.serverMapList = []

			// if(!this.isfirst) return ;
			let _this=this;

			// 2d地图加载后
			this.view.when(v=>{
			    list.forEach((x,i)=>{
					// if(x.classes != 'water') {
						let Graphic1 = new this.esri.Graphic({
						    geometry: {
						        type: 'point',
						        x: x.longitude,
						        y: x.latitude,
						        spatialReference:v.spatialReference
						    },
							symbol: {
							    type: 'picture-marker',
								url: x.classes == 'flood' ? _this.setFloodIcon(x) : './static/marker/' + x.classes + '.png',
							    width: '67px',
							    height: '95px',
							    params: x.classes,
								classes: x.classes,
								data: x,
							},
							id: x.id
						});
						this.serverMapList.push(Graphic1)
					// }

			    });
				this.serverMapListCopy = JSON.parse(JSON.stringify(this.serverMapList))
				if(this.serverMapList.length > 0) {
					//显示图标
					this.view.graphics.addMany(this.serverMapList);
					if(this.isfirst) this.isfirst=false;
				}
			});

			// 读取事件图标集合
			this.getTimeEvent()
		},
		setFloodIcon(data) {
			return data.level >= 1 ? './static/marker/flood-' + data.level + '.png' : './static/marker/flood-0.png'
		},
		spotEvent(type, w, n) {
			this.circleW = n;
			this.removegGhicsLayer()
			if (type == 'flood') {
				this.addCircleGeometry(this.floodGraphic, n)
			} else {
				this.addCircleGeometry(this.wGraphic, n)
			}
			this.spotW = w;
		},
		getData() {
			// 获取全图层所有元素POI信息列表
			this.$axios.elements().then((res) => {
				if (res.resultCode === 200) {
					this.elementList = res.data
					// console.log(this.elementList)
					// window.sessionStorage.setItem('data', JSON.stringify(this.elementList))

					let arr = []
					Object.keys(this.elementList).forEach(k => {
						this.elementList[k].forEach((i) => {
							arr.push(i)
						})
					})

					let tempList = this.getXY(arr)
					this.mapList = this.iconExistence(this.mapSignList, tempList)
					this.setMapIconList(this.mapList)
					this.setMapIcon(this.iconsIsCheck(this.mapSignList, this.mapList))
				}
			})
		},
		// 保存所有图标集合信息
		setMapIconList(list) {
			this.allServerMapList = []

			let _this=this;

			// 2d地图加载后
			this.view.when(v=>{
				list.forEach((x,i)=>{
					let Graphic1 = new this.esri.Graphic({
						geometry: {
							type: 'point',
							x: x.longitude,
							y: x.latitude,
							spatialReference:v.spatialReference
						},
						symbol: {
							type: 'picture-marker',
							url: x.classes == 'flood' ? _this.setFloodIcon(x) : './static/marker/' + x.classes + '.png',
							width: '67px',
							height: '95px',
							params: x.classes,
							classes: x.classes,
							data: x,
						},
						id: x.id
					});
					this.allServerMapList.push(Graphic1)
				});
			});
		},
		// 判断图标是否存在
		iconExistence(typeList, mapList) {
			let tempList = []
			mapList.forEach((t, k) => {
				typeList.forEach((m) => {
					m.Children.forEach((c) => {
						if (t.classes == c.classes) {
							tempList.push(mapList[k])
						}

					})
				})
			})
			return tempList
		},
		// 判断图标是否选中
		iconsIsCheck(typeList, mapList) {
			let tempList = []
			mapList.forEach((t, k) => {
				typeList.forEach((m) => {
					m.Children.forEach((c) => {
						if(t.classes == 'video'){
							if(t.splb == c.splb && t.classes == c.classes && c.isSelect){
								tempList.push(mapList[k])
							}
						}else{
							if (t.classes == c.classes && c.isSelect) {
								tempList.push(mapList[k])
							}
						}
					})
				})
			})
			return tempList
		},
		getRainfallNum(list) {
			let temp = []
			if (list.length > 0) {
				list.forEach((d) => {
					if (d.rainfall > 0) {
						temp.push(d)
					}
				})
			}
			return temp
		},
		onWaterRegime(index) {
			this.active1 = index
			if (this.active1 == 0) {
				this.tableList = this.reservoirPoiList
			} else if (this.active1 == 1) {
				this.tableList = this.riverPoiList
			} else if (this.active1 == 2) {
				this.tableList = this.floodPoiList
			}
		},
		warnEvent(type, v, t) {
			if (t == 0) {
				this.warningLevelList = this.reservoirPoiList.filter(o=>o.level>0)
			} else if (t == 1) {
				this.warningLevelList = this.riverPoiList.filter(o=>o.level>0)
			} else if (t == 2) {
				this.warningLevelList = this.floodPoiList.filter(o=>o.level>0)
			} else if (t == 3) {
				this.warningLevelList = this.rainfallPoiList.filter(o=>o.rainfall>0)
			} else if (t == 4) {
				this.warningLevelList = this.rainfallHourList.filter(o=>o.level>0)
			}
			this.leftV = this.scaleV * v + 'px';
			this.isPopover = true
		},
		tebleEvent() {
		    this.isPopover = false
		},
		// 全选
		changeEvent(index) {

			this.mapSignList[index].Children.forEach((m) => {
				m.isSelect = this.mapSignList[index].isSelect
				// if (n.symbol.classes == m.classes) {
				// 	n.visible = this.mapSignList[index].isSelect
				//
				// }
			})

			this.setMapIcon(this.iconsIsCheck(this.mapSignList, this.mapList))

			this.serverMapListCopy = JSON.parse(JSON.stringify(this.serverMapList))
		},
		// 单选
		cutSelEvent(item) {
			this.setMapIcon(this.iconsIsCheck(this.mapSignList, this.mapList))

			// console.log(item)
			// this.serverMapList.forEach((m) => {
			// 	if (m.symbol.classes == 'video') {
			// 		if (m.symbol.data.splb == item.splb) {
			// 			m.visible = item.isSelect
			// 		}
			// 	} else {
			// 		if (m.symbol.classes == item.classes ) {
			// 			m.visible = item.isSelect
			// 		}
			// 	}
			//
			// })
			// this.serverMapListCopy = JSON.parse(JSON.stringify(this.serverMapList))
		},
		superpositionEvent(e) {
			this.isSuperposition = e
			// this.show_dialog[this.mapTypeIndex] = false
		},
		stopFlash() {
		    if (this.graphicTiem) {
		        clearInterval(this.graphicTiem);
		        this.graphicTiem = null;
		    }
		},
		homeReturnEvent() {
			this.navTxt = '首页'
			this.removegGhicsLayer()
			this.visibleIcon();
		},
		//移除对应的graphicsLayer
		removegGhicsLayer() {
			//this.stopFlash();
			let layer = this.indexMap.layers.items;
			if(this.warningGraphic) this.circleGraphicsLayer.remove(this.warningGraphic);
		},
		visibleIcon() {
			this.setMapIcon(this.iconsIsCheck(this.mapSignList, this.mapList))

			// this.serverMapList.forEach((i) => {
			// 	i.visible = false
			// 	this.mapSignList.forEach((m) => {
			// 		m.Children.forEach(n=>{
			// 			if (i.symbol.classes == 'video') {
			//
			// 			} else {
			// 				if (i.symbol.classes== n.classes && n.isSelect) {
			// 					i.visible = n.isSelect
			// 					// n.isSelect = true
			// 				}
			// 			}
			// 		})
			// 	})
			//
			// })
		},
		eliminateEvent() {
			this.mapSignList.forEach((m) => {
				m.Children.forEach(n=>{
					n.isSelect = false
				})
			})
			this.serverMapList.forEach((i) => {
				i.visible = false
			})
		}

	},
}
